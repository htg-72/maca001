import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'rooms.dart';
import 'micro.dart';
import 'matrix.dart';
import 'main.dart';

Future<Card> dooMatrixGetHistory(Client client, String roomId) async {
  final roomF = client.getRoomById(roomId);
  final timeline = await roomF!.getTimeline();
  List<Event> events = [];

  String response = '';
  String responseVal = '';
  String request = '';


  await timeline
      .requestHistory(historyCount: 100)
      .timeout(const Duration(seconds: 5));
  events = timeline.events.where((e) => e.type == EventTypes.Message).toList();

  for (int i = 0; i < events.length; i++) {
    //print("HISTORY EVENT: ${events[i].content["body"]}");
    //dooMatrixGetResponse(client, events[i]);

    if (events[i].content["body"] != null &&
        !events[i].content["body"].contains("STATUS")) {
      if (events[i].content["m.relates_to"] != null &&
          !events[i].content["body"].contains("STATUS")) {
        if (events[i].content["m.relates_to"]["m.in_reply_to"] != null) {
          String replyFull = events[i].content["body"];
          List<String> replyLines = replyFull.split("\n");
          response = replyLines[replyLines.length - 1];
          if (response != '') {
            print('RESPONSE: ${response}');
          }
        }
      } else {
        request = events[i].content["body"];
        if (request != '') {
          print('REQUEST: ${request}');
        }
      }
    }

    if (events[i].content["body"] != null) {
      if (events[i].content["m.relates_to"] != null &&
          !events[i].content["body"].contains("STATUS")) {
        if (events[i].content["m.relates_to"]["m.in_reply_to"] !=
            null) {
          String id = events[i].content["m.relates_to"]
              ["m.in_reply_to"]["event_id"];

          final event2 = await client
              .getRoomById('!tEXPAuGSfTvFCPTtgZ:doostam.org')!
              .getEventById(id);

          String responseVal = event2!.content["body"];
          if (responseVal != '') {
            print('RESPONSE VAL: ${responseVal}');
          }
        }
      }
    }

    if (responseVal == '') {
      return room(request, 'NO RESPONSE!', dooVideoServer(videoServer, 32));
    } else {
      return room(request, responseVal, dooVideoServer(videoServer, 32));
    }
  }
  return room('title', 'desc', 'url');
}

Future<String> dooMatrixGetResponse(Client client, EventUpdate event) async {
  if (event.content["content"]["body"] != null &&
      !event.content["content"]["body"].contains("STATUS")) {
    if (event.content["content"]["m.relates_to"] != null &&
        !event.content["content"]["body"].contains("STATUS")) {
      if (event.content["content"]["m.relates_to"]["m.in_reply_to"] != null) {
        String replyFull = event.content["content"]["body"];
        List<String> replyLines = replyFull.split("\n");
        return 'RESPONSE: ${replyLines[replyLines.length - 1]}';
      }
    } else {
      return 'REQUEST: ${event.content["content"]["body"]}';
    }
  }
  return '';
}

Future<String> dooMatrixGetResponseValue(
    Client client, EventUpdate event) async {
  if (event.content["content"]["body"] != null) {
    if (event.content["content"]["m.relates_to"] != null &&
        !event.content["content"]["body"].contains("STATUS")) {
      if (event.content["content"]["m.relates_to"]["m.in_reply_to"] != null) {
        String id = event.content["content"]["m.relates_to"]["m.in_reply_to"]
            ["event_id"];

        final event2 = await client
            .getRoomById('!tEXPAuGSfTvFCPTtgZ:doostam.org')!
            .getEventById(id);

        return event2!.content["body"];
      }
    }
  }
  return '';
}

class WhoAmIPriv extends StatelessWidget {
  const WhoAmIPriv({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: dooAppBar('Private Translations'),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: DooBigButton(
                    text: 'Receive Translations',
                    image: 'smartphone_video_phone_woman_man.png',
                    callback: PrivReceive(),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: DooBigButton(
                    text: 'Donate Translations',
                    image: 'english_smartphone_honyakuki.png',
                    callback: PrivDonate(),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class PrivDonate extends StatefulWidget {
  const PrivDonate({Key? key}) : super(key: key);

  @override
  State<PrivDonate> createState() => _PrivDonateState();
}

class _PrivDonateState extends State<PrivDonate> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: dooAppBar('Private - Donate Translations'),
      body: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 40),
        children: rooms,
      ),
    );
  }
}

String zhome = 'https://matrix.doostam.org';
String zuser = '@orchestrator:doostam.org';
String zpass = 'tY9xPBQgpkEMRZSz6Z8C';

Future<Card> matGet(BuildContext context) async {
  WidgetsFlutterBinding.ensureInitialized();

  final client = Client(
    'Doostam',
    databaseBuilder: (_) async {
      final dir = await getApplicationSupportDirectory();
      final db = FluffyBoxDatabase('doostam', dir.path);
      await db.open();
      return db;
    },
  );

  await client.init();

  client.onLoginStateChanged.stream.listen((LoginState loginUpdate) {
    //print("LoginState: ${loginUpdate.toString()}");
  });

  client.onEvent.stream.listen((EventUpdate eventUpdate) async {
    String response = await dooMatrixGetResponse(client, eventUpdate);
    String responseVal = await dooMatrixGetResponseValue(client, eventUpdate);
    if (response != '') {
      print(response);
    }
    if (responseVal != '') {
      print("RESPONSE VAL: $responseVal");
    }
  });

  client.onSync.stream.listen((SyncUpdate eventUpdate) {
    //print("New room update!");
  });

  await client.checkHomeserver(zhome);
  await client.login(
    LoginType.mLoginPassword,
    identifier: AuthenticationUserIdentifier(user: zuser),
    password: zpass,
  );

  //await client.getRoomById('!zhKCHEqzMYTvUkapZt:doostam.org')!.sendTextEvent('Hello world');

  String roomId = '!rjeBBjfMXWmgNlVctn:doostam.org';
  await client
      .getRoomById(roomId)!
      .sendTextEvent('STATUS: Orchestrator Logged In');

  return dooMatrixGetHistory(client, roomId);
}

class PrivReceive extends StatelessWidget {
  const PrivReceive({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: dooAppBar('Private - Receive Translations'),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () => matGet(context),
              child: const Text('Load'),
            )
          ],
        ),
      ),
    );
  }
}
