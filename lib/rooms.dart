import 'package:doostam/doo_private.dart';
import 'package:flutter/material.dart';
import 'package:flutter_lorem/flutter_lorem.dart';
import 'package:url_launcher/url_launcher.dart';
import 'main.dart';
import 'micro.dart';

final rooms = List<Card>.generate(
    30,
    //(i) => room(lorem(paragraphs: 1, words: 5), lorem(paragraphs: 1, words: 10),
    //    dooVideoServer(videoServer, 32))

    (i) => room(lorem(paragraphs: 1, words: 5), lorem(paragraphs: 1, words: 10),
      dooVideoServer(videoServer, 32))

    //(i) => i % 6 == 0
    //    ? HeadingItem('Heading $i')
    //    : MessageItem('Sender $i', 'Message body $i'),
    );

//List<Card> roomGen(BuildContext context) async {
//  List<Card> zrooms;
//  for (int i = 0; i < 100; i++) {
//    zrooms.add(await matGet(context));
//  }
//  zrooms;
//}

Card room(String title, String desc, String url) {
  return Card(
    child: Row(
      children: [
        Expanded(
          child: Column(
            children: <Widget>[
              ListTile(
                leading: const Icon(Icons.access_alarms_outlined),
                title: Text(title),
                subtitle: Text(desc),
              ),
            ],
          ),
        ),
        Container(
          alignment: Alignment.centerRight,
          child: Expanded(
            child: Row(
              children: <Widget>[
                TextButton(
                  child: const Text('VIDEO CHAT'),
                  onPressed: () => launch(url),
                ),
                const SizedBox(width: 8),
                TextButton(
                  child: const Text('DISMISS'),
                  onPressed: () {},
                ),
                const SizedBox(width: 8),
              ],
            ),
          ),
        ),
      ],
    ),
  );
}

ListView roomz = ListView.builder(
    padding: const EdgeInsets.all(8),
    itemCount: rooms.length,
    itemBuilder: (BuildContext context, int index) {
      return Container(
        height: 50,
        color: Colors.amber,
        child: rooms[index],
      );
    });
