import 'package:flutter/material.dart';
import 'onboarding.dart';
import 'doo_public.dart';
import 'doo_private.dart';
import 'micro.dart';

// Globals
const videoServer = 'https://video.doostam.org/';
const matrixServer = 'https://matrix.doostam.org/';

void main() {
  runApp(const Doostam());
}

class Doostam extends StatelessWidget {
  const Doostam({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Requests',
      theme: ThemeData(
        primarySwatch: Colors.indigo,
      ),
      home: const OnBoardingPage(),
    );
  }
}

class PubPriv extends StatelessWidget {
  const PubPriv({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: dooAppBar('Doostam.org'),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: DooBigButton(
                    text: 'Public Translations',
                    image: 'keijiban_harigami.png',
                    callback: WhoAmIPub(),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: DooBigButton(
                    text: 'Private Translations',
                    image: 'lock_file.png',
                    callback: WhoAmIPriv(),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
