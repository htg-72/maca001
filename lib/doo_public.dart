import 'package:flutter/material.dart';
import 'rooms.dart';
import 'micro.dart';

class WhoAmIPub extends StatelessWidget {
  const WhoAmIPub({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: dooAppBar('Public Translations'),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: DooBigButton(
                    text: 'Receive Translations',
                    image: 'job_english_tsuuyaku.png',
                    callback: PubReceive(),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: DooBigButton(
                    text: 'Donate Translations',
                    image: 'job_computer_technocrat.png',
                    callback: PubDonate(),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class PubDonate extends StatefulWidget {
  const PubDonate({Key? key,}) : super(key: key);

  @override
  State<PubDonate> createState() => _PubDonateState();
}

class _PubDonateState extends State<PubDonate> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: dooAppBar('Public - Donate Translations'),
      body: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 40),
        children: rooms,
      ),
    );
  }
}

class PubReceive extends StatelessWidget {
  const PubReceive({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: dooAppBar('Public - Receive Translations'),
      body: Center(
        child: ElevatedButton(
          onPressed: () {},
          child: const Text('Go back!'),
        ),
      ),
    );
  }
}
