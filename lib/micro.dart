import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'dart:math';

const _chars = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
Random _rnd = Random();

String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
    length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));

String dooVideoServer(String videoServer, int length) {
  return '$videoServer${getRandomString(length)}';
}

Widget dooImage(String assetName, [double width = 350]) {
  if (assetName == 'job_computer_technocrat.png') {
    width = 250;
  }
  return Image.asset('assets/$assetName', width: width);
}

AppBar dooAppBar(String title) {
  return AppBar(
    centerTitle: true,
    title: dooTextH1(title),
  );
}

Text dooTextH1(String str) {
  return Text(str, style: GoogleFonts.lato());
}

Text dooTextB1(String str) {
  return Text(
    str,
    style: GoogleFonts.lato(
      fontSize: 28.0,
      fontWeight: FontWeight.w700,
      color: Colors.black,
    ),
  );
}

Column navbutton(String text, String image) {
  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      dooTextB1(text),
      dooImage(image),
    ],
  );
}

class DooBigButton extends StatelessWidget {
  final String text;
  final String image;
  final Widget callback;
  const DooBigButton({
    Key? key,
    required this.text,
    required this.image,
    required this.callback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        minimumSize: const Size.fromHeight(50),
        primary: Colors.grey[300],
      ),
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => callback,
          ),
        );
      },
      child: navbutton(text, image),
    );
  }
}
